var express = require('express');
var https = require('https');
var http = require('http');
var fs = require('fs');
var url = require('url');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser());
var options = {
    host: '127.0.0.1',
    key: fs.readFileSync('ssl/server.key'),
    cert: fs.readFileSync('ssl/server.crt')
};
  var cities = fs.readFileSync('cities.dat.txt');

  var basicAuth = require('basic-auth-connect');
  var auth = basicAuth(function(user, pass) {
    return((user ==='cs201r')&&(pass === 'test'));
  });

  http.createServer(app).listen(80);
  https.createServer(options, app).listen(443);
  
  app.use('/', express.static('./html', {maxAge: 60*60*1000}));

  app.get('/getcity', function (req, res) {
    console.log("In getcity route");
    res.json([{city:"Price"},{city:"Provo"}]);
  });

  app.get('/comment', function (req, res) {
    console.log("In comment route");
    console.log(req.body.Name);
    console.log(req.body.Comment);
     var MongoClient = require('mongodb').MongoClient;
      MongoClient.connect("mongodb://localhost/weather", function(err, db) {
        if(err) throw err;
        db.collection("comments", function(err, comments){
          if(err) throw err;
          comments.find(function(err, items){
            items.toArray(function(err, itemArr){
              console.log("Document Array: ");
              console.log(itemArr);
              res.writeHead(200);
              res.end(JSON.stringify(itemArr));
            });
          });
        });
      });
    //res.json(resarray);
  });


  app.post('/comment', auth, function (req, res) {
    console.log("In POST comment route");
    console.log(req.user);
    console.log("Remote User");
    console.log(req.remoteUser);
    console.log(req.body);
    var reqObj = req.body;
    console.log(reqObj);
    console.log("Name: "+reqObj.Name);
    console.log("Comment: "+reqObj.Comment);

    // Now put it into the database
    var MongoClient = require('mongodb').MongoClient;
    MongoClient.connect("mongodb://localhost/weather", function(err, db) {
      if(err) throw err;
      db.collection('comments').insert(reqObj,function(err, records) {
        console.log("Record added as "+records[0]._id);
        //res.status(200);
        res.end();
      });
    });
    
  });

