var mongoose = require('mongoose');
var commentSchema = new mongoose.Schema({
  title: String,
  upvotes: {type: Number, default: 0},
});
mongoose.model('comment', commentSchema);
