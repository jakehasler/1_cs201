var http = require('http');
var ROOT_DIR = "html";
var url = require('url');
var fs = require('fs');

http.createServer(function(req, res)
{
    var urlObj = url.parse(req.url, true, false);
    console.log("opening " + ROOT_DIR + urlObj.pathname);
    console.log(urlObj);
    if(urlObj.pathname == "/getcity")
    {
        res.writeHead(200);
        var myReg = new RegExp("^"+urlObj.query["q"]);
        var jsonResult = [];
        //res.end(JSON.stringify(cities));
        fs.readFile('cities.dat.txt', function(err, data)
        {
            if(err) throw err;
            cities = data.toString().split('\n');
            for(var i = 0; i < cities.length; i++)
            {
                var result = cities[i].search(myReg);
                if(result != -1)
                {
                    jsonResult.push({city:cities[i]});
                }
                
            }
            res.end(JSON.stringify(jsonResult));
        });
        
        console.log("In REST Service");
    }
    else
    {
        fs.readFile(ROOT_DIR + urlObj.pathname, function (err, data)
        {
            if(err)
            {
                res.writeHead(404);
                res.end(JSON.stringify(err));
                return;
            }
            res.writeHead(200);
            res.end(data);
        
        });
    }
    
}).listen(9000);


console.log("Running server");