var main = function() {

    $(".article").click(function(){
        $('.article').removeClass('current');
        $('.description').hide();
        
        $(this).children('.description').show();
        $(this).addClass('current');
        
        });
        
        
    $(document).keypress(function(event){
        if(event.which === 111)
        {
            $('.current').children('.description').toggle();
        }
        else if(event.which === 110)
        {
            var currentArticle = $('.current');
            var nextArticle = currentArticle.next();
            
            currentArticle.removeClass('current');
            nextArticle.addClass('current');
        }
        
        });
 
    
}


$(document).ready(main);


var suggestCity = function(str) {
        var url = "getcity?q="+ $("#cityName").val();
        $.getJSON(url,function(data)
        {
            var cityList = "<ul>";
		
    	   $.each(data, function(i,item) {
            cityList += "<li> " + data[i].city;
            });
		
    		cityList += "</ul>";
    		
    	    $("#suggestions").html(cityList);
        })
			
	    
	    
	    .done(function() { console.log('getJSON request succeeded!'); })
        .fail(function(jqXHR, textStatus, errorThrown) { 
         console.log('getJSON request failed! ' + textStatus); 
        console.log("incoming "+jqXHR.responseText);
         })
        .always(function() { console.log('getJSON request ended!');
         })
        .complete(function() { console.log("complete"); });

		
};
       
       
   $("#btn-submit").click(function(e){
    var value = $("#cityName").val();
    console.log(value);
    $("#selectedCity").html(value);
     e.preventDefault();
    
    // Rest of Ajax call here: 
    var myurl= "https://api.wunderground.com/api/936feb76a9ce7997/geolookup/conditions/q/UT/";
    myurl += value;
    myurl += ".json";
    console.log(myurl);
    $.ajax({
        url : myurl,
        dataType : "jsonp",
        success : function(data) {
            console.log(data);
            var location = data['location']['city'];
              var temp_string = data['current_observation']['temperature_string'];
              var current_weather = data['current_observation']['weather'];
              var everything = "<ul>";
              everything += "<li>Location: "+location;
              everything += "<li>Temperature: "+temp_string;
              everything += "<li>Weather: "+current_weather;
              everything += "</ul>";
              $("#currentWeather").html(everything);
        }
      }
    
    );
    
    
    
    
    
   
    
    });
    
    


var citySubmit = function(submittedCity)
{
    
    
    console.log("Form has been submitted.");
    
    console.log("The city in the form value is: " + submittedCity);
    
    $("#selectedCity").html(submittedCity);
    
    
}