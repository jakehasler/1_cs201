var app = angular.module("jeopardy", []);

app.service("categoryService", function($http, $q)
{
	var deferred = $q.defer();

	http.get('files/questions.json').then(function(data)
    {
        deferred.resolve(data);
    });

	this.getQuestions = function()
	{
		return deferred.promise;
	};

});