app.controller('MainController', ['$scope', '$http', function($scope, $http) {
  //$scope.title = 'Find The Gram'; 
  $scope.secondaryTitle = '';
  $scope.questionText = 'These are the Questions';
  // Object for storing category names and questions
  $scope.category = [
  {
    name: "Category 1",
    questions: [{
                num: "$100",
                questionName: "Name of Question" ,
                answer: "Name of Answer",
                href: '#question1',
                id: 'question1'
                },
                {
                num: "$200",
                questionName: "Name of Question2" ,
                answer: "Name of Answer2",
                href: '#question2',
                id: 'question2'
                },
                {
                num: "$300",
                questionName: "Name of Question3" ,
                answer: "Name of Answer3",
                href: '#question3',
                id: 'question3'
                },
                {
                num: "$400",
                questionName: "Name of Question4" ,
                answer: "Name of Answer4",
                href: '#question4',
                id: 'question4'
                },
                {
                num: "$500",
                questionName: "Name of Question5" ,
                answer: "Name of Answer5",
                href: '#question5',
                id: 'question5'
                }
                ]
  },
  {
    name: "Category 2",
    questions: [{
                num: "$100",
                questionName: "Name of Question" ,
                answer: "Name of Answer",
                href: '#question1',
                id: 'question1'
                },
                {
                num: "$200",
                questionName: "Name of Question2" ,
                answer: "Name of Answer2",
                href: '#question2',
                id: 'question2'
                },
                {
                num: "$300",
                questionName: "Name of Question3" ,
                answer: "Name of Answer3",
                href: '#question3',
                id: 'question3'
                },
                {
                num: "$400",
                questionName: "Name of Question4" ,
                answer: "Name of Answer4",
                href: '#question4',
                id: 'question4'
                },
                {
                num: "$500",
                questionName: "Name of Question5" ,
                answer: "Name of Answer5",
                href: '#question5',
                id: 'question5'
                }
                ]
  },
  {
    name: "Category 3",
    questions: [{
                num: "$100",
                questionName: "Name of Question" ,
                answer: "Name of Answer",
                href: '#question1',
                id: 'question1'
                },
                {
                num: "$200",
                questionName: "Name of Question2" ,
                answer: "Name of Answer2",
                href: '#question2',
                id: 'question2'
                },
                {
                num: "$300",
                questionName: "Name of Question3" ,
                answer: "Name of Answer3",
                href: '#question3',
                id: 'question3'
                },
                {
                num: "$400",
                questionName: "Name of Question4" ,
                answer: "Name of Answer4",
                href: '#question4',
                id: 'question4'
                },
                {
                num: "$500",
                questionName: "Name of Question5" ,
                answer: "Name of Answer5",
                href: '#question5',
                id: 'question5'
                }
                ]
  },
  {
    name: "Category 4",
    questions: [{
                num: "$100",
                questionName: "Name of Question" ,
                answer: "Name of Answer",
                href: '#question1',
                id: 'question1'
                },
                {
                num: "$200",
                questionName: "Name of Question2" ,
                answer: "Name of Answer2",
                href: '#question2',
                id: 'question2'
                },
                {
                num: "$300",
                questionName: "Name of Question3" ,
                answer: "Name of Answer3",
                href: '#question3',
                id: 'question3'
                },
                {
                num: "$400",
                questionName: "Name of Question4" ,
                answer: "Name of Answer4",
                href: '#question4',
                id: 'question4'
                },
                {
                num: "$500",
                questionName: "Name of Question5" ,
                answer: "Name of Answer5",
                href: '#question5',
                id: 'question5'
                }
                ]

  }];


  
  //var promise = categoryService.getQuestions();
  // promise.then(function (data)
  // {
  //       $scope.question = data;
  //       console.log($scope.question);
  // });

  $scope.genCategories = function()
  {
    console.log("The Button has been clicked.");

    $http.get('/files/questions.json').success(function(data)
    {
        console.log(data);
    });

    // $http({
    //     method: 'GET',
    //     url: 'https://api.wunderground.com/api/936feb76a9ce7997/geolookup/conditions/q/UT/Provo.json'
    //   }).then(function successCallback(response) {
    //       // this callback will be called asynchronously
    //       // when the response is available
    //     }, function errorCallback(response) {
    //       // called asynchronously if an error occurs
    //       // or server returns response with an error status.
    //     });




  };

  $scope.toggleAnswer = function() 
  {
      $scope.answer = $scope.category[0].questions[0].answer;
  };

// Perform Ajax call, get items from rest services and reset arrays.
  // $http.get(myurl2).success(function(response)
  // {
  //   $scope.images = response.data;
  // });

  // for(int i)

  $scope.imagesOriginal = [
  	{ 
    	image: 'https://scontent.cdninstagram.com/hphotos-xfp1/t51.2885-15/e15/11203399_806631449426649_2070423860_n.jpg', 
    	location: 'BYU LAUNDRY', 
    	username: 'r7bear'
  	}, 
  	{ 
    	image: 'https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/s640x640/sh0.08/e35/11821282_1705837886302824_998604239_n.jpg', 
      location: 'BYU (Brigham Young University)', 
      username: 'kyleesuebear'
  	}, 
  	{ 
    	image: 'https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/e35/12142557_1648340592049449_1933313300_n.jpg', 
      location: '(Brigham Young University)', 
      username: 'spence_580'
  	}, 
  	{ 
    	image: 'https://scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/s640x640/sh0.08/e35/12132967_533707176781384_1261124492_n.jpg', 
      location: '(Brigham Young University)', 
      username: 'byuhonors'
  	}
  ];



}]);


// Current Plan

// Get ng-click working
// Style Boxes for Categories
// Use ng-repeat for column Boxes
// Solve ajax call with Angular
// figure out how to call another rest service
// Own Node Server.