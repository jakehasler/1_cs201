var fs = require('fs');
var http = require('http');
var url = require('url');
var ROOT_DIR = "files/";

http.createServer(function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');

    if ( req.method === 'OPTIONS' ) {
        res.writeHead(200);
        res.end();
        return;
    }
  var questions;
  var urlObj = url.parse(req.url, true, false);
  if (urlObj.pathname.indexOf("getSingleJeopardy") != -1) {
    var index = Math.floor((Math.random() * questions.length));
    var initQuestion = questions[index];
    while (initQuestion["round"] != "Jeopardy!") {
        index = Math.floor((Math.random() * questions.length));
        initQuestion = questions[index];
    }
    var showQuestions = [];
    for (var i = 0; i < questions.length; i++) {
        if (questions[i]["show_number"] == initQuestion["show_number"]) {
            showQuestions.push(questions[i]);
        }
    }
    var roundQuestions = [];
    for (var i = 0; i < showQuestions.length; i++) {
        if (showQuestions[i]["round"] == initQuestion["round"]) {
            roundQuestions.push(showQuestions[i]);
        }
    }
    var currCategory = [];
    var categories = [];
    var newQuestion = initQuestion;
    while (categories.length < 6) {
        while (currCategory.length < 5) {
            for (var i = 0; i < roundQuestions.length; i++) {
                if (roundQuestions[i]["category"] == newQuestion["category"]) {
                    currCategory.push(roundQuestions[i]);
                    roundQuestions.splice(i,1);
                }
            }
        }
        newQuestion = roundQuestions[Math.floor((Math.random() * roundQuestions.length))]
        categories.push(currCategory);
        currCategory = [];
    }
    console.log("Single Jeopardy Sent");
    res.writeHead(200);
    res.end(JSON.stringify(categories));
      //});
  }
  // else if (urlObj.pathname.indexOf("") != -1) {
  //     fs.readFile(ROOT_DIR + "index.html", function (err,data) {
  //       if (err) {
  //         res.writeHead(404);
  //         res.end(JSON.stringify(err));
  //         return;
  //       }
  //       console.log("Index Page Sent");
  //       res.writeHead(200);
  //       res.end(data);
  //     });
  // }
  else {
      fs.readFile(ROOT_DIR + urlObj.pathname, function (err,data) {
        if (err) {
          res.writeHead(404);
          res.end(JSON.stringify(err));
          return;
        }
        res.writeHead(200);
        res.end(data);
      });
  }
  questions = JSON.parse(fs.readFileSync(ROOT_DIR + 'questions.json'));
}).listen(4004);
console.log("Server listening on port 4004");


/*var options = {
    hostname: 'localhost',
    port: '4004',
    path: '/index.html'
  };
function handleResponse(response) {
  var serverData = '';
  response.on('data', function (chunk) {
    serverData += chunk;
  });
  response.on('end', function () {
    console.log(serverData);
  });
}
http.request(options, function(response){
  handleResponse(response);
}).end();*/
